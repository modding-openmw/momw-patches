#### 50 Skip Intro Videos Fixed

##### About

Replaces the skip intro and logo videos with files that hopefully work on everyone's setup.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes (but there's no need)
* `Rebirth`: Yes

##### Load Order

```
...
data="/home/username/games/openmw/Mods/UserInterface/MonochromeUserInterface/monochrome-user-interface"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/50 Skip Intro Videos Fixed"
...

```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/50%20Skip%20Intro%20Videos%20Fixed)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
