#### 43 Investigations at Tel Eurus Anti-Cheese

Rebalances an item from [Investigations at Tel Eurus](https://www.nexusmods.com/morrowind/mods/51938) to be more in line with other mods from MOMW's modlists.

##### About

Nerfs the final quest reward from Investigations at Tel Eurus from insanely crazy strong to strong and useful.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: Yes

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Quests/InvestigationsatTelEurus/Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/43 Investigations at Tel Eurus Anti-Cheese"¨
...
content=Tel_Eurus.esp
... 
content=Tel Eurus Anticheese.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/43%20Investigations%20at%20Tel%20Eurus%20Anti-Cheese)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)

