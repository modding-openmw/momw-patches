#### 12 Practical Necromancy Vendor Patch

Fixes an issue with a NPC edited by [Practical Necromancy (OpenMW)](https://www.nexusmods.com/morrowind/mods/47390).

##### About

Fixes NPC settings for the necromancer Dedaenc so that he actually sells black soul gems like the mod page says.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Gameplay/PracticalNecromancyOpenMW/Practical Necromancy"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/12 Practical Necromancy Vendor Patch"
...
content=Practical Necromancy.omwaddon
...
content=Practical Necromancy Vendor Patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/12%20Practical%20Necromancy%20Vendor%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
