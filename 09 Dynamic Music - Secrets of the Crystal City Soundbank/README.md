#### 09 Dynamic Music - Secrets of the Crystal City Soundbank

Conversion of the MWSE OST addon for [Secrets of the Crystal City](https://www.nexusmods.com/morrowind/mods/51932) for [Dynamic Music (OpenMW Only)](https://www.nexusmods.com/morrowind/mods/53568).

##### About

This is a soundbank that enables Secrets of the Crystal City's previously MWSE-only soundtrack to work as intended with OpenMW-Lua via [Dynamic Music (OpenMW Only)](https://www.nexusmods.com/morrowind/mods/53568).

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### Load Order

This mod has no special load order requirements.

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/09%20Dynamic%20Music%20-%20Secrets%20of%20the%20Crystal%20City%20Soundbank)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
