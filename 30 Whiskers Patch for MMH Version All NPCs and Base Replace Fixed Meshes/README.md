#### 30 Whiskers Patch for MMH Version - All NPCs and Base Replace Fixed Meshes


##### About

Fixes the `meshes/_khj_replace/Dahnara.nif` and `meshes/_khj_replace/Zahraji.nif` meshes included with **Whiskers Patch for MMH Version - All NPCs and Base Replace** from [Pluginless Khajiit Head Pack](https://www.nexusmods.com/morrowind/mods/43110).

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: Yes

##### Load Order

```
...
data="/home/username/games/openmw/Mods/NPCs/WhiskersPatchforMMHVersionAllNPCsandBaseReplace"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/30 Whiskers Patch for MMH Version All NPCs and Base Replace Fixed Meshes"
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/30%20Whiskers%20Patch%20for%20MMH%20Version%20All%20NPCs%20and%20Base%20Replace%20Fixed%20Meshes)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
