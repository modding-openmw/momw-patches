#### 03 BCoM + Ghastly Glowyfence + Dynamic Distant Buildings for OpenMW Patch

##### About

An edited version of the `HM_DDD_Ghostfence_v1.0.esp` plugin from [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236) that supports [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) and [Ghastly Glowyfence](https://www.nexusmods.com/morrowind/mods/47982).

**NOTE: This replaces the `HM_DDD_Ghostfence_v1.0.esp` plugin from Dynamic Distant Buildings for OpenMW and is meant to be loaded instead of it!**

**NOTE: The `BCOM+GhastlyGG+DynamicDistantBuildingsPatch.esp` plugin has already been cleaned! Some tooling (TES3CMD) may want to delete two fence records that it thinks are dirty. They look dirty, but they are not. If you clean those out this plugin will be broken!**

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No
* `Anything else that edits the related Ghostfence and/or Ghostgate cells`: Unknown, please let me know about compatibility issues!

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Replacers/GhastlyGlowyfence"
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/00 Core"
...
data="/home/username/games/openmw/Mods/Fixes/DynamicDistantBuildingsforOpenMW/Dynamic Distant Details"
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/03 BCOM+Ghastly Glowyfence+Dynamic Distant Buildings Patch"
...
content=Beautiful cities of Morrowind.esp
...
content=ghastly gg.esp
...
content=BCOM+GhastlyGG+DynamicDistantBuildingsPatch.esp
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/03%20BCOM+Ghastly%20Glowyfence+Dynamic%20Distant%20Buildings%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)
