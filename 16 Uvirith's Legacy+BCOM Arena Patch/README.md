#### 16 Uvirith's Legacy + BCoM Arena Patch

**WARNING! Spoilers below!! WARNING!**

(Highlight with your mouse to see on the website, when reading the text file just don't look past this part or do if you want the spoiler!)

##### About

Adjusts the script for one of the quests from [Uvirith's Legacy](https://modding-openmw.com/mods/uviriths-legacy/) (the quest <span class='spoiler'>The Prisoner Felinglith</span>) to account for layout changes of Vivec, Arena Pit done by [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231).

**WARNING! Spoilers above!! WARNING!**

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/00 Core"
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/Patches/43 Uvirith Legacy Patch"
...
data="/home/username/games/openmw/Mods/PlayerHomes/UvirithsLegacy/Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/16 Uvirith's Legacy+BCOM Arena Patch"
...
content=Beautiful cities of Morrowind.ESP
...
content=Uvirith's Legacy_3.53.ESP
...
content=UL_-_BCoM_Arena_Patch.ESP
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/16%20Uvirith's%20Legacy+BCOM%20Arena%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
