#!/bin/sh
set -eu

file_name=momw-patches.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

iconv -t utf-8 -f utf-16 < fomod/ModuleConfig.xml > ModuleConfig.xml
sed -i "s|\.json|\.esp|g;s|\.yaml|\.esp|g" ModuleConfig.xml
iconv -f utf-8 -t utf-16 < ModuleConfig.xml > fomod/ModuleConfig.xml

zip --must-match --recurse-paths \
    $file_name \
    "01 OAAB Grazelands+Mines and Caverns Patch" \
    "02 Lush Synthesis+TotSP Patch" \
    "03 BCOM+Ghastly Glowyfence+Dynamic Distant Buildings Patch" \
    "04 Draggle-Tail Shack Price Adjustment" \
    "05 AFFresh+Samarys Ancestral Tomb Expanded Patch" \
    "06 Cutting Room Floor+BCOM Patch" \
    "07 mtrPermanentQuestCorpsesRandomizer+Shipyards of Vvardenfell Patch" \
    "08 Trackless Grazeland For OpenMW" \
    "09 Dynamic Music - Secrets of the Crystal City Soundbank" \
    "10 Ebonheart Underworks MOMW Patch" \
    "11 Mamaea Awakened OpenMW Patch" \
    "12 Practical Necromancy Vendor Patch" \
    "13 Doors of Oblivion+TOTSP Patch" \
    "14 Mines and Caverns MOMW Patch" \
    "15 Perfectly Proficient Parasol Particles Performance Patch" \
    "16 Uvirith's Legacy+BCOM Arena Patch" \
    "17 Uvirith's Legacy+Daedric Shrine Overhaul Molag Bal Patch" \
    "18 Uvirith's Legacy+Daedric Shrine Overhaul Sheogorath Patch" \
    "19 Uvirith's Legacy+OAAB Tel Mora Patch" \
    "20 Immersive Grotto Entrances Patched" \
    "21 Imperial Factions Patched" \
    "22 BCOM+Quests for Clans and Vampire Legends Patch" \
    "23 Little Landscapes Seyda Neen Swamp Pools Patched" \
    "24 BCOM+Dagon Fel Lighthouse+Remiros' Groundcover Patch" \
    "25 Dynamic Music - Songbook of the North Soundbank" \
    "26 Argonian Full Helms Lore Integrated+Concept Arts plantations+The Wolverine Hall Patch" \
    "27 Little Landscapes Nix Hound Hunting Grounds+Quests for Clans and Vampire Legends Patch" \
    "28 Morrowind Anti-Cheese+Quest Voice Greetings Patch" \
    "29 Better Clothes Complete Mara's Blouse Fix" \
    "30 Whiskers Patch for MMH Version All NPCs and Base Replace Fixed Meshes" \
    "31 Flies Patched" \
    "32 OpenMW Fixes For Uvirith's Legacy" \
    "33 AFFresh+Library of Vivec Enhanced Patch" \
    "34 MOMW Logs On Fire Patch" \
    "35 The Forgotten Shields - Artifacts Patched" \
    "37 Helm of Tohan Naturalized for Daedric Shrine Overhaul Sheogorath" \
    "40 NOD+Library of Vivec Enhanced Patch" \
    "41 Blademeister+Daedric Shrine Overhaul Sheogorath Patch" \
    "42 FMGS Unique Items Compilation Mesh Fix" \
    "43 Investigations at Tel Eurus Anti-Cheese" \
    "44 Daedric Shrine Overhaul Vaermina OpenMW Patch" \
    "46 Mages Guild Stronghold Nchagalelft+BCOM Patch" \
    "47 Vivec's Muatra Alternative" \
    "48 Races RESPECted OpenMW Lua Addon" \
    "49 Aether Pirates Discovery + The Corprusarium Experience Patch" \
    "50 Skip Intro Videos Fixed" \
    "51 Remiros AI MOMW Patch" \
    "52 Telvanni Magister Robes Anticheese" \
    "53 Mages Guild Stronghold Nchagalelft OpenMW Patch" \
    "54 OAAB Brother Junipers Twin Lamps+Marbled Zafirbel Bay Patch" \
    "55 Quest Voice Greetings+Quests for Clans and Vampire Legends Patch" \
    "56 Quest Voice Greetings+Tamriel Data Patch" \
    "57 NOD+Daedric Shrine Overhaul Molag Bal Patch" \
    "58 OAAB Shipwrecks+JFK MOMW Patch" \
    CHANGELOG.md \
    LICENSE \
    README.md \
    version.txt \
    fomod \
    --exclude \*.json \
    --exclude \*.yaml \
    --exclude ./01\ OAAB\ Grazelands+Mines\ and\ Caverns\ Patch/scripts\* \
    --exclude ./03\ BCOM+Ghastly\ Glowyfence+Dynamic\ Distant\ Buildings\ Patch/scripts\* \
    --exclude ./06\ Cutting\ Room\ Floor+BCOM\ Patch/scripts\* \
    --exclude ./07\ mtrPermanentQuestCorpsesRandomizer+Shipyards\ of\ Vvardenfell\ Patch/scripts\* \
    --exclude ./10\ Ebonheart\ Underworks\ MOMW\ Patch/scripts\* \
    --exclude ./11\ Mamaea\ Awakened\ OpenMW\ Patch/scripts\* \
    --exclude ./16\ Uvirith\'s\ Legacy+BCOM\ Arena\ Patch/scripts\* \
    --exclude ./17\ Uvirith\'s\ Legacy+Daedric\ Shrine\ Overhaul\ Molag\ Bal\ Patch/scripts* \
    --exclude ./18\ Uvirith\'s\ Legacy+Daedric\ Shrine\ Overhaul\ Sheogorath\ Patch/scripts* \
    --exclude ./19\ Uvirith\'s\ Legacy+OAAB\ Tel\ Mora\ Patch/scripts\* \
    --exclude ./22\ BCOM+Quests\ for\ Clans\ and\ Vampire\ Legends\ Patch/scripts\* \
    --exclude ./28\ Morrowind\ Anti\-Cheese+Quest\ Voice\ Greetings\ Patch/scripts\* \
    --exclude ./31\ Flies\ Patched/scripts\* \
    --exclude ./32\ OpenMW\ Fixes\ For\ Uvirith\'s\ Legacy/scripts\* \
    --exclude ./33\ AFFresh+Library\ of\ Vivec\ Enhanced\ Patch/scripts\* \
    --exclude ./35\ The\ Forgotten\ Shields\ \-\ Artifacts\ Patched/scripts\* \
    --exclude ./41\ Blademeister+Daedric\ Shrine\ Overhaul\ Sheogorath\ Patch/scripts\* \
    --exclude ./44\ Daedric\ Shrine\ Overhaul\ Vaermina\ OpenMW\ Patch/scripts\* \
    --exclude ./46\ Mages\ Guild\ Stronghold\ Nchagalelft+BCOM\ Patch/scripts\* \
    --exclude ./49\ Aether\ Pirates\ Discovery\ +\ The\ Corprusarium\ Experience\ Patch/scripts\* \
    --exclude ./54\ OAAB\ Brother\ Junipers\ Twin\ Lamps+Marbled\ Zafirbel\ Bay\ Patch/scripts\
sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt
