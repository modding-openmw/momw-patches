#### 46 Mages Guild Stronghold - Nchagalelft + BCoM Patch

Fixes incompatibilities between [Mages Guild Stronghold - Nchagalelft](https://www.nexusmods.com/morrowind/mods/53342) and [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231).

##### About

Fixes the teleporters to the Ald'ruhn and Vivec Guild of Mages in Nchagalelft so that they teleport you to coordinates that are correct for the interior changed by Beautiful Cities of Morrowind.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/00 Core"
...
data="/home/username/games/openmw/Mods/PlayerHomes/MagesGuildStrongholdNchagalelft/Building up Nchagalelft"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/46 Mages Guild Stronghold Nchagalelft+BCOM Patch"
...
content=Beautiful cities of Morrowind.ESP
...
content=MGO Building Up Nchagalelft.esp
content=BCOM_MGOBuildingUpNchagalelft_Patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/46%20Mages%20Guild%20Stronghold%20Nchagalelft+BCOM%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)

