#### 54 OAAB Brother Junipers Twin Lamps + Marbled Zafirbel Bay Patch

Fixes incompatibilities between [OAAB Brother Junipers Twin Lamps](https://www.nexusmods.com/morrowind/mods/51424) and [Marbled Zafirbel Bay](https://www.nexusmods.com/morrowind/mods/53126).

##### About

Moves NPCs spawned during one of the quests from the Twin Lamps mod so they aren't buried under a rock added by Marbled Zafirbel Bay.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
data="/home/username/games/openmw/Mods/Landscape/MarbledZafirbelBay"
...
data="/home/username/games/openmw/Mods/GuildsFactions/OAABBrotherJunipersTwinLamps"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/54 OAAB Brother Junipers Twin Lamps+Marbled Zafirbel Bay Patch"
...
content=Marbled Zafirbel Bay.ESP
...
content=OAAB Brother Junipers Twin Lamps.esp
content=OAABBrotherJunipersTwinLamps_MarbledZafirbelBay_patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/54%20OAAB%20Brother%20Junipers%20Twin%20Lamps+Marbled%20Zafirbel%20Bay%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)

