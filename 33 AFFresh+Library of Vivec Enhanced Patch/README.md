#### 33 AFFresh + Library of Vivec Enhanced Patch

Fixes incompatibilities between [AFFresh](https://www.nexusmods.com/morrowind/mods/53006) and [Library of Vivec Enhanced](https://www.nexusmods.com/morrowind/mods/50181).

##### About

**WARNING! Spoilers below!! WARNING!**

(Highlight with your mouse to see on the website, when reading the text file just don't look past this part or do if you want the spoiler!)

<span class="spoiler">Fixes the placement of the Nchulem book and adjusts scripts so that the teleport from and to the Library of Vivec during the quest The Search for Nchulem works as intended.</span>

**WARNING! Spoilers above!! WARNING!**

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Interiors/LibraryofVivecEnhanced/00 Core"
...
data="/home/username/games/openmw/Mods/Quests/AFFresh"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/33 AFFresh+Library of Vivec Enhanced Patch"
...
content=AFFresh.esp
content=AFFreshLibraryOfVivecEnhanced.esp
...
content=RPNR_Library.ESP
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/33%20AFFresh+Library%20of%20Vivec%20Enhanced%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
