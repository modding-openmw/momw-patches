#### 02 Lush Synthesis + Solstheim Tomb of the Snow Prince Patch

##### About

Regenerated underwater groundcover from [Lush Synthesis](https://www.nexusmods.com/morrowind/mods/52931) to support [Solstheim Tomb of the Snow Prince](https://www.nexusmods.com/morrowind/mods/46810). Requires the original mod.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Groundcover/LushSynthesis"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/02 Lush Synthesis+TotSP Patch"
...
groundcover=LS_Vvardenfell_seas_and_lakes.esp
groundcover=LS_TotSP_seas_and_lakes.esp
groundcover=LS_Vvardenfell_rivers.esp
groundcover=LS_TotSP_rivers.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/02%20Lush%20Synthesis+TotSP%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
