#### 07 mtrPermanentQuestCorpsesRandomizer OpenMW omwaddon + Shipyards of Vvardenfell Patch

##### About

Patches incompatibilities between [mtrPermanentQuestCorpsesRandomizer OpenMW omwaddon - Randomizer for Location of Permanent Corpses of Imperial Taxman Processus Vitellius and Skooma Dealer Ernil Omoran Associated with Quests](https://www.nexusmods.com/morrowind/mods/51375) and [Shipyards of Vvardenfell (Sadrith Mora - Seyda Neen - Gnaar Mok)](https://www.nexusmods.com/morrowind/mods/51928).

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### Load Order

This mod has no special load order requirements.

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/07%20mtrPermanentQuestCorpsesRandomizer+Shipyards%20of%20Vvardenfell%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
