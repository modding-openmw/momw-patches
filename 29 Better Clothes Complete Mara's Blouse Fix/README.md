#### 29 Better Clothes Complete Mara's Blouse Fix


##### About

Fixes an error in [Better Clothes Complete](https://www.nexusmods.com/morrowind/mods/47549) which causes Mara's Blouse to disappear sometimes. The fix replaces the broken unique mesh with a different one.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Clothing/BetterClothesComplete/#Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/29 Better Clothes Complete Mara's Blouse Fix"
...
content=Better Clothes Complete.ESP
...
content=BC_mara_fix.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/29%20Better%20Clothes%20Complete%20Mara's%20Blouse%20Fix)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
