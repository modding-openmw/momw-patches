#### 26 Argonian Full Helms Lore Integrated + Concept Arts plantations + The Wolverine Hall Patch

A patched version of [Argonian Full Helms Lore Integrated and Modders Resource v 1.1](https://www.nexusmods.com/morrowind/mods/53011) that resolves incompatibilities with [Concept Arts plantations](https://www.nexusmods.com/morrowind/mods/50020) and [The Wolverine Hall](https://www.nexusmods.com/morrowind/mods/45194).

##### About

Moves two floating items added by Argonian Full Helms Lore Integrated and Modders Resource v 1.1 to account for changes done by Concept Arts plantations and The Wolverine Hall.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

Use this **instead** of the `Argonian Full Helms Lore Integrated.ESP` plugin that comes with Argonian Full Helms Lore Integrated and Modders Resource v 1.1!

```
...
data="/home/username/games/openmw/Mods/Armor/ArgonianFullHelmsLoreIntegratedandModdersResourcev11/Argonian Full Helms Lore Integrated v1.1/Data Files"
...
data="/home/username/games/openmw/Mods/CitiesTowns/ConceptArtsplantations/00 Core"
...
data="/home/username/games/openmw/Mods/CitiesTowns/TheWolverineHall/01 Core"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/26 Argonian Full Helms Lore Integrated+The Wolverine Hall Patch"
...
content=content=Clean_Argonian Full Helms Lore Integrated.ESP
...
content=Concept_Arts_plantations.esp
...
content=RPNR_WHO.ESP
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/26%20Argonian%20Full%20Helms%20Lore%20Integrated%2BThe%20Wolverine%20Hall%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
