#### 52 Telvanni Magister Robes Anticheese

##### About

Removes robe chests and balances the crimson and purple robe.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: Yes

##### Load Order

```
...
data="/home/username/games/openmw/Mods/ObjectsClutter/TelvanniMagisterRobes/Telvanni Magister Robes-52278-1-25-1683240241/Telvanni Magister robes/Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/52 Telvanni Magister Robes Anticheese"
...
content=Telvanni Magister Robes.esp
...
content=Telvanni Magister Robes Anticheese.esp
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/52%20Telvanni%20Magister%20Robes%20Anticheese)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
