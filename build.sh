#!/bin/sh
set -e
IFS=$(echo "\n\b")
for path in $(find . -maxdepth 1 -mindepth 1 -type d | grep -Ev "\.git|web|fomod"); do
    cd "$path"
    for yaml in $(find . -type f -name "*.yaml"); do
        delta_plugin convert "$yaml"
        plugin=$(echo "$yaml" | sed "s|yaml|omwaddon|")
        mv "$plugin" "$(echo "$plugin" | sed "s|omwaddon|esp|g")"
    done
    cd ..
done

for json in $(find . -type f -name "*.json"); do
    cd "$(dirname "$json")"
    jsonFile="$(find . -type f -name "*.json")"
    tes3conv "$jsonFile" "$(echo "$jsonFile" | sed "s|json|esp|")"
    cd ..
done

mv "20 Immersive Grotto Entrances Patched/IGE.esp" "20 Immersive Grotto Entrances Patched/IGE.ESP"
mv "21 Imperial Factions Patched/Imperial Factions.esp" "21 Imperial Factions Patched/Imperial Factions.ESP"
mv "23 Little Landscapes Seyda Neen Swamp Pools Patched/Little Landscapes - Seyda Neen Swamp Pools.esp" "23 Little Landscapes Seyda Neen Swamp Pools Patched/Little Landscapes - Seyda Neen Swamp Pools.ESP"
mv "24 BCOM+Dagon Fel Lighthouse+Remiros' Groundcover Patch/REM_AC.esp" "24 BCOM+Dagon Fel Lighthouse+Remiros' Groundcover Patch/REM_AC.ESP"
mv "26 Argonian Full Helms Lore Integrated+Concept Arts plantations+The Wolverine Hall Patch/Clean_Argonian Full Helms Lore Integrated.esp" "26 Argonian Full Helms Lore Integrated+Concept Arts plantations+The Wolverine Hall Patch/Clean_Argonian Full Helms Lore Integrated.ESP"
mv "31 Flies Patched/Flies.esp" "31 Flies Patched/Flies.ESP"
mv "47 Vivec's Muatra Alternative/RC_VivecsMuatra.esp" "47 Vivec's Muatra Alternative/RC_VivecsMuatra.ESP"
mv "51 Remiros AI MOMW Patch/Rem_AI.esp" "51 Remiros AI MOMW Patch/Rem_AI.ESP"
