#### 22 BCOM+Quests for Clans and Vampire Legends Patch

Fixes incompatibilities between [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) and [Quests for Clans and Vampire Legends (QCVL)](https://www.nexusmods.com/morrowind/mods/49486).

##### About

Moves one NPC added by QVCL out of a wall in Vivec, Black Shalk Cornerclub.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/00 Core"
...
data="/home/username/games/openmw/Mods/GuildsFactions/QuestsforClansandVampireLegendsQCVL/Quests for Clans and Vampire Legends/Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/22 BCOM+Quests for Clans and Vampire Legends Patch"
...
content=Quests for Clans and Vampire Legends.ESP
...
content=QCVL_BCoM_fix.esp
...
content=Beautiful cities of Morrowind.ESP
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/22%20BCOM+Quests%20for%20Clans%20and%20Vampire%20Legends%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
