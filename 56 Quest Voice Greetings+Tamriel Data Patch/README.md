#### 56 Quest Voice Greetings + Tamriel Data Patch

Fixes incompatibilities between [Quest Voice Greetings](https://www.nexusmods.com/morrowind/mods/52273) and [Tamriel Data](https://www.nexusmods.com/morrowind/mods/44537).

##### About

Quest Voice Greetings adds new greetings for the three vanilla vampire clans. However, Tamriel Data (and by extension, the PTR province mods) adds new vampire clans for which the greetings aren't filtered to, meaning the new vampire NPCs default to greetings meant for non-vampire NPCs. This patch duplicates the QVG greetings for the new TD clans.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
data="/home/username/games/openmw/Mods/ModdingResources/TamrielData"
...
data="/home/username/games/openmw/Mods/Audio/QuestVoiceGreetings/Quest Voice Greetings 3.82"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/56 Quest Voice Greetings+Tamriel Data Patch"
...
content=Tamriel_Data.esm
...
content=Quest Voice Greetings.ESP
...
content=QuestVoiceGreetings_TamrielData_patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/56%20Quest%20Voice%20Greetings+Tamriel%20Data%20Patch?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)

