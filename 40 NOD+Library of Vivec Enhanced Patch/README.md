#### 40 NOD + Library of Vivec Enhanced Patch

Fixes an incompatibility between [NOD - NPC Outfit Diversity](https://www.nexusmods.com/morrowind/mods/52091) and [Library of Vivec Enhanced](https://www.nexusmods.com/morrowind/mods/50181).

##### About

Moves the position of one of the Ordinator guards added by NOD so that he isn't spawned inside the library walls anymore.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/NPCs/NODNPCOutfitDiversity/00 Core"
...
data="/home/username/games/openmw/Mods/Interiors/LibraryofVivecEnhanced/00 Core"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/40 NOD+Library of Vivec Enhanced Patch"
...
content=NOD - Core.esm
...
content=RPNR_Library.ESP
content=NOD_RPNR_Library_patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/40%20NOD%2BLibrary%20of%20Vivec%20Enhanced%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
