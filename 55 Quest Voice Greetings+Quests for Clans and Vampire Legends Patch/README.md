#### 55 Quest Voice Greetings + Quests for Clans and Vampire Legends Patch

Fixes incompatibilities between [Quest Voice Greetings](https://www.nexusmods.com/morrowind/mods/52273) and [Quests for Clans and Vampire Legends (QCVL)](https://www.nexusmods.com/morrowind/mods/49486).

##### About

Quest Voice Greetings adds new greetings for the three vanilla vampire clans. However, Quests for Clans and Vampire Legends (QCVL) adds new offshoots of the vampire clans for which the greetings aren't filtered to, meaning the new vampire NPCs default to greetings meant for non-vampire NPCs. This patch duplicates the QVG greetings for the new QCVL clans.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
data="/home/username/games/openmw/Mods/Audio/QuestVoiceGreetings/Quest Voice Greetings 3.82"
...
data="/home/username/games/openmw/Mods/GuildsFactions/QuestsforClansandVampireLegendsQCVL/Quests for Clans and Vampire Legends/Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/55 Quest Voice Greetings+Quests for Clans and Vampire Legends Patch"
...
content=Quests for Clans and Vampire Legends.ESP
...
content=Quest Voice Greetings.ESP
...
content=QuestVoiceGreetings_QCVL_patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/55%20Quest%20Voice%20Greetings+Quests%20for%20Clans%20and%20Vampire%20Legends%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)

