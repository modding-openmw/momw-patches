#### 41 Blademeister + Daedric Shrine Overhaul Sheogorath Patch

Fixes incompatibilities between [Blademeister](https://www.nexusmods.com/morrowind/mods/52740) and [Daedric Shrine Overhaul FULL](https://www.nexusmods.com/morrowind/mods/54679).

##### About

Moves one of the quest item chests added by Blademeister to not be obstructed by statics in one of the Daedric shrines edited by Daedric Shrine Overhaul Sheogorath.

Works with both the full version of Daedric Shrine Overhaul and the standalone [Daedric Shrine Overhaul Sheogorath](https://www.nexusmods.com/morrowind/mods/51890) mod.¨

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CavesandDungeons/DaedricShrineOverhaulFULL/Daedric Shrine Overhaul FULL"
...
data="/home/username/games/openmw/Mods/Companions/Blademeister/Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/41 Blademeister+Daedric Shrine Overhaul Sheogorath Patch"
...
content=Blademeister_v1.5.esp
...
content=Daedric Shrine Overhaul FULL.esp
content=BlademeisterDaedricShrineOverhaulSheogorathPatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/41%20Blademeister%2BDaedric%20Shrine%20Overhaul%20Sheogorath%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
