#### 47 Vivec's Muatra Alternative

A replacer for the [Vivec's Muatra](https://www.nexusmods.com/morrowind/mods/53342) mod, tweaking some of its aspects.

##### About

Changes Vivec so that he doesn't respawn anymore and has the essential flag again.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/47 Vivec's Muatra Alternative"
...
content=RC_VivecsMuatra.ESP
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/47%20Vivec's%20Muatra%20Alternative)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)

