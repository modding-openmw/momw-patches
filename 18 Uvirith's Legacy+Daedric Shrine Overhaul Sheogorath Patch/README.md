#### 18 Uvirith's Legacy + Daedric Shrine Overhaul Sheogorath Patch

Moves a few journal pages and books added to the Sheogorath shrines by [Uvirith's Legacy](https://modding-openmw.com/mods/uviriths-legacy/) to a position adjusted for the layout changes done by [Daedric Shrine Overhaul FULL](https://www.nexusmods.com/morrowind/mods/54679). Without the patch, the items are floating in the air.

##### About

Works with all versions of Uvirith's Legacy (BCoM/original).

Works with both the full version of Daedric Shrine Overhaul and the standalone [Daedric Shrine Overhaul Sheogorath](https://www.nexusmods.com/morrowind/mods/51890) mod.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CavesandDungeons/DaedricShrineOverhaulFULL/Daedric Shrine Overhaul FULL"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/18 Uvirith's Legacy+Daedric Shrine Overhaul Sheogorath Patch"
...
content=Daedric Shrine Overhaul FULL.esp
content=UL_-_Daedric_Shrine_Overhaul_Sheogorath.ESP
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/18%20Uvirith's%20Legacy+Daedric%20Shrine%20Overhaul%20Sheogorath%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
