#### 51 Remiros AI patch (BCOM, Concept Art Plantations, Great Seawall of Vivec, Little Landscapes - Path to Vivec Lighthouse, Vivec Lighthouse)

##### About

Mega patch to fix grass issues in the titled mods. Only use this version of Remiros AI and no others if you use all of the above mods.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Groundcover/RemirosGroundcover/00 Core OpenMW"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/51 Remiros AI patch (BCOM, Concept Art Plantations, Great Seawall of Vivec, Little Landscapes - Path to Vivec Lighthouse, Vivec Lighthouse)"
...
content=Rem_AI.ESP
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/51%20Remiros%20AI%20patch%20(BCOM,%20Concept%20Art%20Plantations,%20Great%20Seawall%20of%20Vivec,%20Little%20Landscapes%20-%20Path%20to%20Vivec%20Lighthouse,%20Vivec%20Lighthouse))
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
