#### 44 Daedric Shrine Overhaul Vaermina OpenMW Patch

A patch for the Vaermina shrine in the [Daedric Shrine Overhaul FULL](https://www.nexusmods.com/morrowind/mods/54679) mod with a workaround for OpenMW incompatibility.

##### About

The mod's functionality is dependent on a one-way collision mesh which doesn't work in OpenMW. As as a workaround, this patch adds a scripted teleporter that teleports you past the mesh if you get close to it. 

Works with both the full version of Daedric Shrine Overhaul and the standalone [Daedric Shrine Overhaul Vaermina](https://www.nexusmods.com/morrowind/mods/53342) mod.¨

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes (but there's no point)
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CavesandDungeons/DaedricShrineOverhaulFULL/Daedric Shrine Overhaul FULL"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/44 Daedric Shrine Overhaul Vaermina OpenMW Patch"
...
content=Daedric Shrine Overhaul FULL.esp
content=Daedric Shrine Overhaul Vaermina OpenMW Patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/44%20Daedric%20Shrine%20Overhaul%20Vaermina%20OpenMW%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
