#### 20 Immersive Grotto Entrances Patched

A patched version of the plugin for the mod [Immersive Grotto Entrances](https://www.nexusmods.com/morrowind/mods/54014).

##### About

Restores deleted unique Koal Cave door to fix the *Lead the Pilgrim to Koal Cave* quest.
`
**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Landscape/ImmersiveGrottoEntrances"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/20 Immersive Grotto Entrances Patched"
...
content=IGE.ESP
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/20%20Immersive%20Grotto%20Entrances%20Patched)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
