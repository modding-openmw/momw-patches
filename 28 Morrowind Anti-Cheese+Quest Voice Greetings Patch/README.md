#### 28 Morrowind Anti-Cheese + Quest Voice Greetings Patch

Fixes incompatibilities between [Morrowind Anti-Cheese](https://www.nexusmods.com/morrowind/mods/47305) and [Quest Voice Greetings](https://www.nexusmods.com/morrowind/mods/52273).

##### About

Should work with all versions of Morrowind Anti-Cheese, including the version included in [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231).

**WARNING! Spoilers below!! WARNING!**

(Highlight with your mouse to see on the website, when reading the text file just don't look past this part or do if you want the spoiler!)

<span class="spoiler">Removes the Cracked Limewire Platter item added by Morrowind Anti-Cheese from the character creation sequence, so that there aren't multiple Limewire Platters.</span>

**WARNING! Spoilers above!! WARNING!**

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Gameplay/MorrowindAntiCheese"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/28 Morrowind Anti-Cheese+Quest Voice Greetings Patch"
...
content=Morrowind Anti-Cheese.ESP
...
content=Morrowind_Anti-CheeseQuest_Voice_Greetings_patch.ESP
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/28%20Morrowind%20Anti-Cheese+Quest%20Voice%20Greetings%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
