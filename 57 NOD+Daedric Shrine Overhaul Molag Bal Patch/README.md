#### 57 NOD+Daedric Shrine Overhaul Molag Bal Patch

Fixes an incompatibility between [NOD - NPC Outfit Diversity](https://www.nexusmods.com/morrowind/mods/52091) and [Daedric Shrine Overhaul FULL](https://www.nexusmods.com/morrowind/mods/54679).

##### About

Moves the position of a helmet added by NOD so that it doesn't float as a result of the changes made by Daedric Shrine Overhaul. Also works with the standalone version of the Molag Bal overhaul.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/NPCs/NODNPCOutfitDiversity/00 Core"
...
data="/home/username/games/openmw/Mods/CavesandDungeons/DaedricShrineOverhaulFULL/Daedric Shrine Overhaul FULL"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/57 NOD+Daedric Shrine Overhaul Molag Bal Patch"
...
content=NOD - Core.esm
...
content=Daedric Shrine Overhaul FULL.esp
...
content=NOD_DaedricShrineOverhaul_patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/57%20NOD+Daedric%20Shrine%20Overhaul%20Molag%20Bal%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
