#### 10 Ebonheart Underworks MOMW Patch

Auto selects some options for [Ebonheart Underworks](https://www.nexusmods.com/morrowind/mods/47272).

##### About

This changes a script to auto deselect the "Rebirth" option.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Quests/EbonheartUnderworks"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/10 Ebonheart Underworks MOMW Patch"
...
content=Ebonheart_Underworks.esp
content=EbonheartUnderworksMOMWPatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/10%20Ebonheart%20Underworks%20MOMW%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
