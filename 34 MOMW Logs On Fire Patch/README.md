#### MOMW Logs on Fire Patch

Fixes an incompatibility between [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231), specifically the Taller Seyda Neen Lighthouse option, and [Logs on Fire](https://www.nexusmods.com/morrowind/mods/51752) as well as incompatibilities with [Solstheim Tomb of the Snow Prince](https://www.nexusmods.com/morrowind/mods/4681, [Mines and Caverns](https://www.nexusmods.com/morrowind/mods/44893), and [OAAB - Tombs and Towers](https://www.nexusmods.com/morrowind/mods/49131).

##### About

Replaces the ESP from the original Logs on Fire mod. Requires [Apel's Fire Retexture Patched for OpenMW and Morrowind Rebirth](https://www.nexusmods.com/morrowind/mods/50092), or the original [Apel's Fire Retexture](https://www.nexusmods.com/morrowind/mods/42554).

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Landscape/ApelsFireRetexturePatchedforOpenMWandMorrowindRebirth"
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/00 Core"
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/02 Options/07 Taller Seyda Neen Lighthouse"
...
data="/home/username/games/openmw/Mods/Lighting/LogsonFire/00 Core"
data="/home/username/games/openmw/Mods/Lighting/LogsonFire/01 HD Textures"
data="/home/username/games/openmw/Mods/Lighting/LogsonFire/02 Apel's Fire Retexture Patch"
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/34 MOMW Logs On Fire Patch"
...
content=Beautiful cities of Morrowind.ESP
...
content=Logs on Fire.esp
...
content=BCOM - Taller Lighthouse.ESP
...
```
The rest of the load between other mods should not matter.

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/34%20MOMW%20Logs%20On%20Fire%20Patch?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
