#### 42 FMGS - Unique Items Compilation Mesh Fix

Updates the Dagger of Symmachus mesh from [FMGS - Unique Items Compilation](https://www.nexusmods.com/morrowind/mods/52740) for better compatibility with OpenMW.

##### About

Removes the bump map texture path and deletes NiSourceTexture effects from the mesh so that the item displays properly in your inventory window.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes (but there's no point)
* `Rebirth`: Yes

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Clothing/FMUniqueItemsCompilation/Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/42 FMGS Unique Items Compilation Mesh Fix"
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/42%20FMGS%20Unique%20Items%20Compilation%20Mesh%20Fix)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
