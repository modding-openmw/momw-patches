#### 04 Draggle-Tail Price Adjustment

Balance the price of [Draggle-Tail Shack](https://www.nexusmods.com/morrowind/mods/53305) for [Total Overhaul](https://modding-openmw.com/lists/total-overhaul/) and similar setups.

##### About

Changes the cost of the Draggle-Tail Shack from 1500 gold to 500 to better fit with the economy when using mods like [For The Right Price](https://www.nexusmods.com/morrowind/mods/52668).

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/PlayerHomes/DraggleTailShack"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/04 Draggle-Tail Shack Price Adjustment"
...
content=Draggle-Tail Shack.ESP
content=DraggleTailPriceAdjustment.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/04%20Draggle-Tail%20Shack%20Price%20Adjustment)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
