#### 19 Uvirith's Legacy + OAAB Tel Mora Patch

**WARNING! Spoilers below!! WARNING!**

(Highlight with your mouse to see on the website, when reading the text file just don't look past this part or do if you want the spoiler!)

##### About

Moves an NPC involved in one of the quests from [Uvirith's Legacy](https://modding-openmw.com/mods/uviriths-legacy/) (the quest <span class="spoiler">Dratha's Dremora</span>) to a position adjusted for the layout changes of Tel Mora, Upper Tower interior done by [OAAB Tel Mora](https://www.nexusmods.com/morrowind/mods/46177).

**WARNING! Spoilers above!! WARNING!**

Tested with the Uvirith's Legacy ESP from [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) Patches, but it should work even with the original Uvirith's Legacy v3.53.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CitiesTowns/OAABTelMora/00 Core"
...
data="/home/username/games/openmw/Mods/PlayerHomes/UvirithsLegacy/Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/19 Uvirith's Legacy+OAAB Tel Mora Patch"
...
content=Uvirith's Legacy_3.53.ESP
...
content=OAAB_Tel Mora.ESP
content=UL_-_OAAB_Tel_Mora_Patch.ESP
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/19%20Uvirith's%20Legacy+OAAB%20Tel%20Mora%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
