# MOMW Patches

Patches made by the Modding-OpenMW.com community.

#### Credits

**Authors**:

- Hurdrax Custos
- johnnyhostile
- Ronik
- S3ctor
- Sophie
- gonzo

**Special Thanks**:

* acidzebra for making [Lush Synthesis](https://www.nexusmods.com/morrowind/mods/52931)
* AFFA for making [AFFresh](https://www.nexusmods.com/morrowind/mods/53006)
* ashiraniir for making [Pluginless Khajiit Head Pack](https://www.nexusmods.com/morrowind/mods/43110)
* Ashstaar for making [The Doors of Oblivion](https://www.nexusmods.com/morrowind/mods/44398)
* Benjamin Winger for making [Delta Plugin](https://gitlab.com/bmwinger/delta-plugin/)
* Billyfighter for making [Aether Pirate's Discovery](https://www.nexusmods.com/morrowind/mods/53320)
* Brother Juniper for making [OAAB Brother Junipers Twin Lamps](https://www.nexusmods.com/morrowind/mods/51424)
* CiceroTR for making [Cutting Room Floor](https://www.nexusmods.com/morrowind/mods/47307)
* Dagoth_Vulgtm for making [Argonian Full Helms Lore Integrated and Modders Resource v 1.1](https://www.nexusmods.com/morrowind/mods/53011)
* Danae for making [Imperial Factions](https://www.nexusmods.com/morrowind/mods/49855)
* DisQualia for making [Races RESPECted - A Race Skill and Attribute Rebalance Mod](https://www.nexusmods.com/morrowind/mods/52967)
* DuoDinamico aka RandomPal and Vegetto for making [Concept Art plantations](https://www.nexusmods.com/morrowind/mods/50020), [Justice for Khartag (J.F.K.)](https://www.nexusmods.com/morrowind/mods/49832), [Library of Vivec Enhanced](https://www.nexusmods.com/morrowind/mods/50181), [The Corprusarium experience](https://www.nexusmods.com/morrowind/mods/49738), and [The Wolverine Hall](https://www.nexusmods.com/morrowind/mods/52199)
* eddie5 for making the [Uvirith's Legacy 3.5 TR add-on 2101](https://modding-openmw.com/mods/uviriths-legacy-35-tr-add-on-2101/)
* eMOElein for making [Dynamic Music (OpenMW Only)](https://www.nexusmods.com/morrowind/mods/53568)
* ffann1998 for making the [Imperial Factions](https://www.nexusmods.com/morrowind/mods/49855) patch
* Glittergear for making [Daedric Shrine Overhaul FULL](https://www.nexusmods.com/morrowind/mods/54679), [Little Landscapes - Nix Hound Hunting Grounds](https://www.nexusmods.com/morrowind/mods/53333), [Little Landscapes - Path to Vivec Lighthouse](https://www.nexusmods.com/morrowind/mods/53352), [Little Landscapes - Seyda Neen Swamp Pools](https://www.nexusmods.com/morrowind/mods/53335), and [Mages Guild Stronghold - Nchagalelft](https://www.nexusmods.com/morrowind/mods/53342)
* Greatness7 for making [tes3conv](https://github.com/Greatness7/tes3conv)
* Half11 for making [Morrowind Anti-Cheese](https://www.nexusmods.com/morrowind/mods/47305) and the [Flies](https://www.nexusmods.com/morrowind/mods/43481) patch
* Hemaris for making [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236)
* havx for contributing to [OpenMW Fixes For Uvirith's Legacy](https://forum.openmw.org/viewtopic.php?p=65466#p65466)
* imsobadatnicknames for making [Songbook of the North - A music mod for Skyrim Home of the Nords](https://www.nexusmods.com/morrowind/mods/46936)
* jple for making the skip intro video used in [Vvardenfell Animated Main Menu](https://www.nexusmods.com/morrowind/mods/54671)
* jsp25 for making [Mines and Caverns](https://www.nexusmods.com/morrowind/mods/44893)
* Katya Karrel for making [Draggle-Tail Shack](https://www.nexusmods.com/morrowind/mods/53305)
* Kalinter for making [The Great Seawall of Vivec](https://www.nexusmods.com/morrowind/mods/53544)
* Lord Zarcon for making [Marbled Zafirbel Bay](https://www.nexusmods.com/morrowind/mods/53126)
* Lougian for making [Netch Bump Mapped](https://www.nexusmods.com/morrowind/mods/42851)
* Lucevar for making [NOD - NPC Outfit Diversity](https://www.nexusmods.com/morrowind/mods/52091) and [OAAB Brother Junipers Twin Lamps](https://www.nexusmods.com/morrowind/mods/51424)
* Melchior Dahrk for making [OAAB Grazelands](https://www.nexusmods.com/morrowind/mods/49075) and [OAAB Tel Mora](https://www.nexusmods.com/morrowind/mods/46177)
* MTR for making [mtrPermanentQuestCorpsesRandomizer OpenMW omwaddon - Randomizer for Location of Permanent Corpses of Imperial Taxman Processus Vitellius and Skooma Dealer Ernil Omoran Associated with Quests](https://www.nexusmods.com/morrowind/mods/51375)
* MwGek for making [Dagon Fel Lighthouse](https://www.nexusmods.com/morrowind/mods/52291), [Logs on Fire](https://www.nexusmods.com/morrowind/mods/51752), and [Vivec Lighthouse](https://www.nexusmods.com/morrowind/mods/52019)
* Naufragous77 for making [OAAB Shipwrecks](https://www.nexusmods.com/morrowind/mods/51364)
* Painkiller_Rider for making [The Forgotten Shields - Artifacts](https://www.nexusmods.com/morrowind/mods/42112)
* PikachunoTM for making [Bethesda Official Plugins Naturalized](https://www.nexusmods.com/morrowind/mods/51107) and [Samarys Ancestral Tomb Expanded](https://www.nexusmods.com/morrowind/mods/45612)
* Poodlesandwich for making [FMGS - Unique Items Compilation](https://www.nexusmods.com/morrowind/mods/46433)
* Queenkeely for [Quests for Clans and Vampire Legends (QCVL)](https://www.nexusmods.com/morrowind/mods/49486)
* R-Zero for making [Flies](https://www.nexusmods.com/morrowind/mods/43481) and [Trackless Grazeland](https://www.nexusmods.com/morrowind/mods/44194)
* Randolph Carter for making [Vivec's Muatra](https://www.nexusmods.com/morrowind/mods/49945)
* RandomPal for making [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231)
* Remiros for making [Morrowind Anti-Cheese](https://www.nexusmods.com/morrowind/mods/47305) and [Remiros' Groundcover](https://www.nexusmods.com/morrowind/mods/46733)
* revenorror for their work on many many things, including [Perfectly Proficient Parasol Particles Performance Patch](https://web.archive.org/web/20221119164736/https://www.nexusmods.com/morrowind/mods/48923)
* rotouns for making [Uvirith's Legacy 3.5 TR add-on 1807](https://modding-openmw.com/mods/uviriths-legacy-35-tr-add-on-1807/)
* RuffinVangarr for making [Telvanni Magister Robes](https://www.nexusmods.com/morrowind/mods/52278)
* Seth for making [Practical Necromancy (OpenMW)](https://www.nexusmods.com/morrowind/mods/47390)
* ShadowMimicry for making [SM Bitter Coast Tree Replacer](https://www.nexusmods.com/morrowind/mods/49883)
* Stuporstar for making [Uvirith's Legacy](https://stuporstar.sarahdimento.com/)
* superduple for making [Blademeister](https://www.nexusmods.com/morrowind/mods/52740)
* SpiritTauren for making [Quests for Clans and Vampire Legends (QCVL)](https://www.nexusmods.com/morrowind/mods/49486)
* Taddeus for making [Necessities of Morrowind](https://www.nexusmods.com/morrowind/mods/52158)
* Von Djangos for making [Quest Voice Greetings](https://www.nexusmods.com/morrowind/mods/52273)
* wazabear for making [Ghastly Glowyfence](https://www.nexusmods.com/morrowind/mods/47982)
* Xandaros for making [OpenMW Fixes For Uvirith's Legacy](https://forum.openmw.org/viewtopic.php?p=65466#p65466)
* Team Ancestral Ashkhans for making [Secrets of the Crystal City](https://www.nexusmods.com/morrowind/mods/51932)
* Team Better Clothes for making [Better Clothes Complete](https://www.nexusmods.com/morrowind/mods/47549)
* Team Dandy Daedra for making [Ebonheart Underworks](https://www.nexusmods.com/morrowind/mods/47272)
* Team Dreamy Dreamers for making [Investigations at Tel Eurus](https://www.nexusmods.com/morrowind/mods/51938)
* Team Flamboyant Armigers for making [Mamaea Awakened](https://www.nexusmods.com/morrowind/mods/46096)
* Team Kwamakaze Kagouti for making [Terror of Tel Amur](https://www.nexusmods.com/morrowind/mods/53673)
* Team Sexy Slippery Sloads for making [Shipyards of Vvardenfell (Sadrith Mora - Seyda Neen - Gnaar Mok)](https://www.nexusmods.com/morrowind/mods/51928)
* The Project Tamriel Team for making [Skyrim Home of the Nords](https://www.nexusmods.com/morrowind/mods/44921)
* The Project Tamriel Rebuilt Team for making [Tamriel_Data](https://www.nexusmods.com/morrowind/mods/44537)
* The Tamriel Rebuilt Team for making [Tamriel Rebuilt](https://www.tamriel-rebuilt.org/)
* The TOTSP Team for making [Solstheim - Tomb of the Snow Prince](https://www.nexusmods.com/morrowind/mods/46810)
* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind and [Helm of Tohan Plugin](https://www.nexusmods.com/morrowind/mods/42987)

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/momw-patches/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/momw-patches)

#### Installation

1. Download the zip from a link above.
1. **Note that included images for `BCOM+GhastlyGG+DynamicDistantBuildingsPatch` contain plot/quest spoilers!**
1. Extract the contents to a location of your choosing.
1. See each mod's individual `README.md` file for detailed load order information.
1. Note that these plugins won't be of any use without their dependent plugins, and these need to load after them. Enjoy!

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/issues/new) for bug reports or feature requests
* Email: `johnnyhostile at modding-openmw dot com`
<!-- * Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/#TODO?tab=posts) -->
