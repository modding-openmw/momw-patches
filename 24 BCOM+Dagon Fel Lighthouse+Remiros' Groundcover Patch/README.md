#### 24 BCOM + Dagon Fel Lighthouse + Remiros' Groundcover patch

Patched groundcover for [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231), [Dagon Fel Lighthouse](https://www.nexusmods.com/morrowind/mods/52291), and [Remiros' Groundcover](https://www.nexusmods.com/morrowind/mods/46733).

##### About

Requires the grass meshes and textures from the original [Remiros' Groundcover](https://www.nexusmods.com/morrowind/mods/46733) mod to work.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Groundcover/RemirosGroundcover/00 Core OpenMW"

data="/home/username/games/openmw/Mods/Patches/MOMWPatches/24 BCOM+Dagon Fel Lighthouse+Remiros' Groundcover Patch"
...
groundcover=REM_AC.ESP
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/24%20BCOM+Dagon%20Fel%20Lighthouse+Remiros'%20Groundcover%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
