#### 35 The Forgotten Shields - Artifacts Patched

Minor balance changes for [The Forgotten Shields - Artifacts](https://www.nexusmods.com/morrowind/mods/42112).

##### About

Removes several of the new artifact shields from leveled lists, so you can only find them in specific dungeons. 

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Replacers/TheForgottenShieldsArtifacts/TheForgottenShields-Artifacts1.3"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/35 The Forgotten Shields - Artifacts Patched"
...
content=TheForgottenShields - Artifacts_NG.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/35%20The%20Forgotten%20Shields%20-%20Artifacts%20Patched)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
