#### 23 Little Landscapes - Seyda Neen Swamp Pools + SM Bitter Coast Tree Replacer Compatibility Patch

Removes some clipping trees from [Little Landscapes - Seyda Neen Swamp Pools](https://www.nexusmods.com/morrowind/mods/53335) when used with [SM Bitter Coast Tree Replacer](https://www.nexusmods.com/morrowind/mods/49883)

##### About

The original Little Landscapes - Seyda Neen Swamp Pools mod is not required.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

There are no special load order requirements.

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/23%20Little%20Landscapes%20Seyda%20Neen%20Swamp%20Pools%20Patched)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
