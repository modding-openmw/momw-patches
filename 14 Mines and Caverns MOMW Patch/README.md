#### 14 Mines and Caverns MOMW Patch

Fixes an incompatibility between [Mines and Caverns](https://www.nexusmods.com/morrowind/mods/44893) and a few mods from MOMW's Total Overhaul modlist (and others).

##### About

Restores the water height and ambient lighting values for the Nissintu cave from Mines and Caverns that are being overwritten with later mods down the list since it would have been too difficult to resolve the conflict in any other way.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: Yes

##### Load Order

Load this near the bottom of your load order.

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/14%20BCOM+Mines%20and%20Caverns)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
