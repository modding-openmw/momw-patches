#### 58 OAAB Shipwrecks+Justice for Khartag MOMW Patch

Fixes a compatibility issue between [Justice for Khartag (J.F.K.)](https://www.nexusmods.com/morrowind/mods/49832) and [OAAB Shipwrecks](https://www.nexusmods.com/morrowind/mods/51364) for the Total Overhaul and Expanded Vanilla modlists.

##### About

From our testing, this patch is the only way to make the two related mods fully compatible in the MOMW modlists without any land tears or duplicate items.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Landscape/OAABShipwrecks/00 Core"
...
data="/home/username/games/openmw/Mods/Landscape/JusticeforKhartagJFK"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/58 OAAB Shipwrecks+JFK MOMW Patch"
...
content=OAAB - Shipwrecks.ESP
content=Justice4Khartag.ESP
content=OAAB_Shipwrecks_JFK_patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/44%20Daedric%20Shrine%20Overhaul%20Vaermina%20OpenMW%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
