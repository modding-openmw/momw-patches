#### 49 Aether Pirate's Discovery + The Corprusarium Experience Patch

Addon for [Aether Pirate's Discovery](https://www.nexusmods.com/morrowind/mods/53320) and [The Corprusarium experience](https://www.nexusmods.com/morrowind/mods/49738) that fixes the placement of a quest item.

##### About

Moves a quest item so that the player can actually find and take it. Adds a small code block to the existing MWScript that was already attached to that item.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes (Aether Pirate's Discovery is not known to be well-supported on this platform)
* `Rebirth`: Yes

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Quests/AetherPiratesDiscovery"
...
data="/home/username/games/openmw/Mods/TheCorprusariumexperience/00 - Core"
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/49 Aether Pirates Discovery + The Corprusarium Experience Patch"
...
content=Aether Pirate's Discovery.omwaddon
...
content=The_corprusarium_Exp.ESP
content=AetherPiratesDiscoveryCorprusariumExperiencePatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/49%20Aether%20Pirates%20Discovery%20+%20The%20Corprusarium%20Experience%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
