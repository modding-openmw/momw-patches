#### 31 Flies Patched


##### About

A patched version of the mod [Flies](https://www.nexusmods.com/morrowind/mods/43481) by [R-Zero](https://www.nexusmods.com/morrowind/users/3241081) that prevents the flies from appearing underwater.

Author of the patch: **[Half11](https://www.nexusmods.com/morrowind/users/36879320)**

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: Yes

##### Load Order

This is a complete replacer for the original mod which includes all of the required files. You don't need the original mod.

```
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/31 Flies Patched"
...
content=Flies.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/31%20Flies%20Patched)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
