#### 11 Mamaea Awakened OpenMW Patch

Patches a script from [Mamaea Awakened](https://www.nexusmods.com/morrowind/mods/46096) to work better with OpenMW.

##### About

Lowers the rotations per second from 200 to 30 so that it plays more nicely with OpenMW's async physics. Also works with the version of the mod included in [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231).

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CavesAndDungeons/MamaeaAwakened"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/11 Mamaea Awakened OpenMW Patch"
...
content=Mamaea Awakened.ESP
content=MamaeaAwakenedOpenMWPatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/11%20Mamaea%20Awakened%20OpenMW%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
