#!/bin/sh
set -eu
#
# This script builds the project website. It downloads soupault as needed and
# then runs it, the built website can be found at: web/build
#

this_dir=$(realpath "$(dirname "${0}")")
cd "${this_dir}"

soupault_version=4.5.0
soupault_pkg=soupault-${soupault_version}-linux-x86_64.tar.gz
soupault_path=./soupault-${soupault_version}-linux-x86_64

shas=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/sha256sums
spdl=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/${soupault_pkg}

if ! [ -f ${soupault_path}/soupault ]; then
    wget ${shas}
    wget ${spdl}
    tar xf ${soupault_pkg}
    grep linux sha256sums | sha256sum -c -
fi

cp ../CHANGELOG.md site/changelog.md
cp ../README.md site/readme.md

{
    cat ../01\ OAAB\ Grazelands+Mines\ and\ Caverns\ Patch/README.md
    cat ../02\ Lush\ Synthesis+TotSP\ Patch/README.md
    cat ../03\ BCOM+Ghastly\ Glowyfence+Dynamic\ Distant\ Buildings\ Patch/README.md
    cat ../04\ Draggle-Tail\ Shack\ Price\ Adjustment/README.md
    cat ../05\ AFFresh+Samarys\ Ancestral\ Tomb\ Expanded\ Patch/README.md
    cat ../06\ Cutting\ Room\ Floor+BCOM\ Patch/README.md
    cat ../07\ mtrPermanentQuestCorpsesRandomizer+Shipyards\ of\ Vvardenfell\ Patch/README.md
    cat ../08\ Trackless\ Grazeland\ For\ OpenMW/README.md
    cat ../09\ Dynamic\ Music\ \-\ Secrets\ of\ the\ Crystal\ City\ Soundbank/README.md
    cat ../10\ Ebonheart\ Underworks\ MOMW\ Patch/README.md
    cat ../11\ Mamaea\ Awakened\ OpenMW\ Patch/README.md
    cat ../12\ Practical\ Necromancy\ Vendor\ Patch/README.md
    cat ../13\ Doors\ of\ Oblivion+TOTSP\ Patch/README.md
    cat ../14\ Mines\ and\ Caverns\ MOMW\ Patch/README.md
    cat ../15\ Perfectly\ Proficient\ Parasol\ Particles\ Performance\ Patch/README.md
    cat ../16\ Uvirith\'s\ Legacy+BCOM\ Arena\ Patch/README.md
    cat ../17\ Uvirith\'s\ Legacy+Daedric\ Shrine\ Overhaul\ Molag\ Bal\ Patch/README.md
    cat ../18\ Uvirith\'s\ Legacy+Daedric\ Shrine\ Overhaul\ Sheogorath\ Patch/README.md
    cat ../19\ Uvirith\'s\ Legacy+OAAB\ Tel\ Mora\ Patch/README.md
    cat ../20\ Immersive\ Grotto\ Entrances\ Patched/README.md
    cat ../21\ Imperial\ Factions\ Patched/README.md
    cat ../22\ BCOM+Quests\ for\ Clans\ and\ Vampire\ Legends\ Patch/README.md
    cat ../23\ Little\ Landscapes\ Seyda\ Neen\ Swamp\ Pools\ Patched/README.md
    cat ../24\ BCOM+Dagon\ Fel\ Lighthouse+Remiros\'\ Groundcover\ Patch/README.md
    cat ../25\ Dynamic\ Music\ \-\ Songbook\ of\ the\ North\ Soundbank/README.md
    cat ../26\ Argonian\ Full\ Helms\ Lore\ Integrated+Concept\ Arts\ plantations+The\ Wolverine\ Hall\ Patch/README.md
    cat ../27\ Little\ Landscapes\ Nix\ Hound\ Hunting\ \Grounds+Quests\ for\ Clans\ and\ Vampire\ Legends\ Patch/README.md
    cat ../28\ Morrowind\ Anti\-Cheese+Quest\ Voice\ Greetings\ Patch/README.md
    cat ../29\ Better\ Clothes\ Complete\ Mara\'s\ Blouse\ Fix/README.md
    cat ../30\ Whiskers\ Patch\ for\ MMH\ Version\ All\ NPCs\ and\ Base\ Replace\ Fixed\ Meshes/README.md
    cat ../31\ Flies\ Patched/README.md
    cat ../32\ OpenMW\ Fixes\ For\ Uvirith\'s\ Legacy/README.md
    cat ../33\ AFFresh+Library\ of\ Vivec\ Enhanced\ Patch/README.md
    cat ../34\ MOMW\ Logs\ On\ Fire\ Patch/README.md
    cat ../35\ The\ Forgotten\ Shields\ \-\ Artifacts\ Patched/README.md
    cat ../37\ Helm\ of\ Tohan\ Naturalized\ for\ Daedric\ Shrine\ Overhaul\ Sheogorath/README.md
    cat ../40\ NOD+Library\ of\ Vivec\ Enhanced\ Patch/README.md
    cat ../41\ Blademeister+Daedric\ Shrine\ Overhaul\ Sheogorath\ Patch/README.md
    cat ../42\ FMGS\ Unique\ Items\ Compilation\ Mesh\ Fix/README.md
    cat ../43\ Investigations\ at\ Tel\ Eurus\ Anti\-Cheese/README.md
    cat ../44\ Daedric\ Shrine\ Overhaul\ Vaermina\ OpenMW\ Patch/README.md
    cat ../46\ Mages\ Guild\ Stronghold\ Nchagalelft+BCOM\ Patch/README.md
    cat ../47\ Vivec\'s\ Muatra\ Alternative/README.md
    cat ../48\ Races\ RESPECted\ OpenMW\ Lua\ Addon/README.md
    cat ../49\ Aether\ Pirates\ Discovery\ +\ The\ Corprusarium\ Experience\ Patch/README.md
    cat ../50\ Skip\ Intro\ Videos\ Fixed/README.md
    cat ../51\ Remiros\ AI\ MOMW\ Patch/README.md
    cat ../52\ Telvanni\ Magister\ Robes\ Anticheese/README.md
    cat ../53\ Mages\ Guild\ Stronghold\ Nchagalelft\ OpenMW\ Patch/README.md
    cat ../54\ OAAB\ Brother\ Junipers\ Twin\ Lamps+Marbled\ Zafirbel\ Bay\ Patch/README.md
    cat ../55\ Quest\ Voice\ Greetings+Quests\ for\ Clans\ and\ Vampire\ Legends\ Patch/README.md
    cat ../56\ Quest\ Voice\ Greetings+Tamriel\ Data\ Patch/README.md
    cat ../57\ NOD+Daedric\ Shrine\ Overhaul\ Molag\ Bal\ Patch/README.md
    cat ../58\ OAAB\ Shipwrecks+JFK\ MOMW\ Patch/README.md
} | grep -Ev "^\[Nexus|^\[Patch Home" >> site/readme.md

cp ../03\ BCOM+Ghastly\ Glowyfence+Dynamic\ Distant\ Buildings\ Patch/BCOM+GhastlyGG+DynamicDistantBuildingsPatch1.png site/img/

PATH=${soupault_path}:$PATH soupault "$@"
