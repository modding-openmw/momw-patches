#### 37 Helm of Tohan Naturalized for Daedric Shrine Overhaul Sheogorath

Edited version of [Helm of Tohan Plugin](https://www.nexusmods.com/morrowind/mods/42987) by Bethesda, based on the Naturalized version from [Bethesda Official Plugins Naturalized](https://www.nexusmods.com/morrowind/mods/51107) and edited for compatibility with [Daedric Shrine Overhaul Sheogorath](https://www.nexusmods.com/morrowind/mods/51890).

##### About

Do not run other versions of Helm of Tohan alongside this one, whether it's going to be the original version, the version from [Unofficial Morrowind Official Plugins Patched](https://www.nexusmods.com/morrowind/mods/43931), the version from Bethesda Official Plugins Naturalized, or any other versions!

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CavesandDungeons/DaedricShrineOverhaulSheogorath"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/37 Helm of Tohan Naturalized for Daedric Shrine Overhaul Sheogorath"
...
content=Daedric Shrine Overhaul Sheogorath.esp
...
content=Helm of Tohan Naturalized.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/37%20Helm%20of%20Tohan%20Naturalized%20for%20Daedric%20Shrine%20Overhaul%20Sheogorath)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
