#### 13 The Doors of Oblivion + Solstheim - Tomb of the Snow Prince Patch

A patch for [Solstheim - Tomb of the Snow Prince](https://www.nexusmods.com/morrowind/mods/46810) and [The Doors of Oblivion](https://www.nexusmods.com/morrowind/mods/44398).

##### About

This was previously distributed by revenorror and rescued from Oblivion by this project.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Landmasses/SolstheimTomboftheSnowPrince/000 Core"
data="/home/username/games/openmw/Mods/Landmasses/SolstheimTomboftheSnowPrince/010 Solstheim - Tomb of the Snow Prince"
...
data="/home/username/games/openmw/Mods/Quests/TheDoorsofOblivion/The Doors of Oblivion 1.4 quest fix/data files"
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/13 Doors of Oblivion+TOTSP Patch"
...
content=Solstheim Tomb of The Snow Prince.esm
...
content=The Doors of Oblivion 1.4.esp
content=Doors of Oblivion+TOTSP Patch.ESP
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/13%20Doors%20of%20Oblivion+TOTSP%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
