local types = require('openmw.types')
local self = require('openmw.self')
local ui = require('openmw.ui')
local core = require('openmw.core')
local input = require('openmw.input')
local time = require("openmw_aux.time")
local ran = false

-- "Yell at user if they are not on 0.49"
local is049orNewer = core.API_REVISION >= 51
if not is049orNewer then
	error("Please download OpenMW version 0.49 to use the Races Respected Argonian Patch")
end

local function onSave()
	return { ran = ran }
end

local function onLoad(data)
	ran = data.ran
end

local function patchArgonian()
    if ran or not input.getControlSwitch(input.CONTROL_SWITCH.ViewMode) then return end
    local correctRace = "argonian"
    local race = types.NPC.record(self).race
    ran = true

    if correctRace ~= race then
        return
    end

    if types.NPC.record(self).isMale then
        types.Player.stats.skills.athletics(self).base = types.Player.stats.skills.athletics(self).base + 5
        types.Player.stats.skills.unarmored(self).base = types.Player.stats.skills.unarmored(self).base + 5
        types.Player.stats.skills.spear(self).base = types.Player.stats.skills.spear(self).base + 5
    else
        types.Player.stats.skills.alchemy(self).base = types.Player.stats.skills.alchemy(self).base + 5
        types.Player.stats.skills.illusion(self).base = types.Player.stats.skills.illusion(self).base + 5
        types.Player.stats.skills.mysticism(self).base = types.Player.stats.skills.mysticism(self).base + 5
    end

    ui.showMessage("Races REspected Argonian Patch Applied")
end

local stopPatch = time.runRepeatedly(
	patchArgonian, 1 * time.second
)

if ran then
	stopPatch()
end

return {
	engineHandlers = {
		onSave = onSave,
		onLoad = onLoad,
	}
}