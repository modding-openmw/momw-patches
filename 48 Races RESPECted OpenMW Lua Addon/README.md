#### 48 Races RESPECted OpenMW-Lua Addon

Addon for [Races RESPECted - A Race Skill and Attribute Rebalance Mod](https://www.nexusmods.com/morrowind/mods/52967) that adds the missing functionality from the MWSE version using OpenMW-Lua. **Requires the OpenMW 0.49 dev build.**

##### About

Implements the Argonian portion of the MWSE addon, but not the "make Caius more buff" portion, of the addon in OpenMW-Lua.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: Yes

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Gameplay/RacesRESPECtedARaceSkillandAttributeRebalanceMod/Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/48 Races RESPECted OpenMW Lua Addon"
...
content=RacesRESPECted.esp
content=racesrespected.omwscripts
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/48%20Races%20RESPECted%20OpenMW%20Lua%20Addon)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)

