## MOMW Patches Changelog

#### 3.17 (unreleased)

* Added `22 BCOM+Quests for Clans and Vampire Legends Patch` by Ronik
* Added `23 Little Landscapes - Seyda Neen Swamp Pools + SM Bitter Coast Tree Replacer Compatibility Patch` by Sophie
* Removed `23 Dynamic Music - Terror of Tel Amur Soundbank` as it"s no longer being used

<!--[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/35353944)-->

#### 3.16.1

* Updated `34 Sophie's Logs On Fire+BCOM Patch` for improved compatibility with Mines and Caverns and renamed to `34 MOMW Logs On Fire Patch` by Sophie and Ronik

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/35483328)

#### 3.16

* Added `20 Immersive Grotto Entrances` by Hurdrax Custos and Ronik

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/35353944)

#### 3.15

* Added `02 Lush Synthesis+TotSP Patch` by Ronik
* Removed `02 Uvirith's Legacy+TR Addon Patched` as it's not used anymore
* Updated `34 Sophie's Logs On Fire+BCOM Patch` for improved compatibility with Solstheim Tomb of the Snow Prince

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/35225862)

#### 3.14

* Added `01 OAAB Grazelands+Mines and Caverns Patch` by Ronik
* Fixed FOMOD installer for `55 Quest Voice Greetings+Quests for Clans and Vampire Legends Patch`

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/34830933)

#### 3.13.1

* Removed `01 BCOM+Imperial Towns Revamp Patch` as it's not used anymore
* Removed `22 AFFresh+Adanumuran Reclaimed Patch` as it's not used anymore
* Removed `36 Blademeister+Adanumuran Reclaimed Patch` as it's not used anymore
* Removed `38 PPL Negative Light Fix` as it's not used anymore
* Removed `39 Signposts Retextured TR Updated` as it's not used anymore
* Removed `45 Wares_containers Kwama Egg Fix` as the issue was fixed upstream
* Removed `59 Dynamic Music - Project Cyrodiil Soundbanks` as it's now included in the Nexus version of the mod

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/34299951)

#### 3.13

* Removed `20 Netch Bump Mapped Mesh Fix` as it's no longer supported

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/34140025)

#### 3.12.1

* Updated `17 Uvirith's Legacy+Daedric Shrine Overhaul Molag Bal Patch` to work with non-BCoM version of Uvirith's Legacy
* Updated `18 Uvirith's Legacy+Daedric Shrine Overhaul Sheogorath Patch` to work with non-BCoM version of Uvirith's Legacy

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/33776448)

#### 3.12

* Added `59 Dynamic Music - Project Cyrodiil Soundbanks` by Ronik

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/33688037)

#### 3.11

* Added `58 OAAB Shipwrecks+JFK MOMW Patch` by Ronik

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/32548607)

#### 3.10

* Added `57 NOD+Daedric Shrine Overhaul Molag Bal Patch` by Ronik
* Updated `34 Sophie's Logs On Fire+BCOM Patch` for compatibility with the Fadathram mod
* Updated `14 Mines and Caverns MOMW Patch` to add a mesh missing from the current version of Mines and Caverns

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/32603686)

#### 3.9.1

* Renamed patch 51 to `51 Remiros AI MOMW Patch`

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/32548607)

#### 3.9

* Added `55 Quest Voice Greetings+Quests for Clans and Vampire Legends Patch` by Ronik
* Added `56 Quest Voice Greetings+Tamriel Data Patch` by Ronik

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/32420749)

#### 3.8

* Added `54 OAAB Brother Junipers Twin Lamps+Marbled Zafirbel Bay Patch` by Ronik

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/30581164)

#### 3.7

* Added `53 Mages Guild Stronghold Nchagalelft OpenMW Patch` by Ronik

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/29881409)

#### 3.6.1

* Fixed an issue with `52 Telvanni Magister Robes Anticheese` 

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/29604308)

#### 3.6

* Added `52 Telvanni Magister Robes Anticheese` by Sophie
* Added `51 Remiros AI patch (BCOM, Concept Art Plantations, Great Seawall of Vivec, Little Landscapes - Path to Vivec Lighthouse, Vivec Lighthouse)` by Sophie
* Added `50 Skip Intro Videos Fixed` by Sophie

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/29541388)

#### 3.5

* Added `12 Practical Necromancy Vendor Patch` by Ronik

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/28577971)

#### 3.4.1

* Updated `23 Dynamic Music - Terror of Tel Amur Soundbank` with missing cells

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/28141948)

#### 3.4

* Added `06 Cutting Room Floor+BCOM Patch` by Ronik

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/27564221)

#### 3.3.2

* Updated `21 Imperial Factions Patched` to fix Skink-in-Tree's-Shade's dialogue when your rank isn't high enough to get the next quest from him

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/26632825)

#### 3.3.1

* Updated `21 Imperial Factions Patched` to fix Aengoth the Jeweler's dialogue when your rank isn't high enough to get the next quest from him

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/26401091)

#### 3.3

* Moved `06 AFFresh+Caldera Mine Expanded Patch` and `12 AFFresh+BCOM Patch` to MOMW Patches Graveyard since the patches are no longer required after AFFresh's latest update
* Moved `44 Daedric Shrine Overhaul Exteriors Mehrunes Dagon and Molag Bal Fix` to MOMW Patches Graveyard since it's not needed for the merged version of Daedric Shrine Overhaul
* Updated `05 AFFresh+Samarys Ancestral Tomb Expanded Patch` for the latest update of AFFresh
* Added `44 Daedric Shrine Overhaul Vaermina OpenMW Patch` by Ronik
* Added `48 Races RESPECted OpenMW Lua Addon` by Sophie
* Added `49 Aether Pirate's Discovery + The Corprusarium Experience Patch` by johnnyhostile

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/26377543)

#### 3.2

* Added `44 Daedric Shrine Overhaul Exteriors Mehrunes Dagon and Molag Bal Fix` by Ronik
* Replaced `29 Better Clothes Complete Shoe Fix` with `29 Better Clothes Complete Mara's Blouse Fix` by Ronik, since the Shoe Fix is now included with the upstream version of Better Clothes Complete
* Updated the trap in `37 Helm of Tohan Naturalized for Daedric Shrine Overhaul Sheogorath` to work in the vanilla engine
* Added `45 Wares_containers Kwama Egg Fix` by gonzo
* Added `46 Mages Guild Stronghold - Nchagalelft + BCoM Patch` by Ronik
* Added `47 Vivec's Muatra Alternative` by Sophie

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/25073404)

#### 3.1

* Added `42 FMGS Unique Items Compilation Mesh Fix` by Sophie
* Added `43 Investigations at Tel Eurus Anti-Cheese` by Sophie

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/24495678)

#### 3.0

* Renamed the project to "MOMW Patches" to better reflect the goal and contents (it's not just for Total Overhaul!)
* Several patches were removed:
  * `01 Redaynia Restored+Mushroom Tree Replacer Patch`
  * `06 OAABShipwrecks+RRBetterShipsnBoatsPatch`
* `02 BCOM+Imperial Towns Revamp Patch` is now `01 BCOM+Imperial Towns Revamp Patch`
* Added a fixed ITR mesh to `01 BCOM+Imperial Towns Revamp Patch` by Sophie
* `03 UvirithsLegacy+TR Patch` is now `02 Uvirith's Legacy+TR Addon Patched`, and is now standalone
* `05 BCOM+GhastlyGG+DynamicDistantBuildingsPatch` is now `03 BCOM+Ghastly Glowyfence+Dynamic Distant Buildings Patch`
* Added `04 Draggle-Tail Shack Price Adjustment` to balance the price of Draggle-Tail Shack for Total Overhaul and similar setups
* Added `05 AFFresh+Samarys Ancestral Tomb Expanded Patch`
* Added `06 AFFresh+Caldera Mine Expanded Patch` by Ronik
* Added `07 mtrPermanentQuestCorpsesRandomizer+Shipyards of Vvardenfell Patch`
* Added `08 Trackless Grazeland For OpenMW`
* Added `09 Dynamic Music - Secrets of the Crystal City Soundbank` by Ronik
* Added `10 Ebonheart Underworks MOMW Patch`
* Added `11 Mamaea Awakened OpenMW Patch` by Hurdrax Custos and johnnyhostile
* Added `12 AFFresh+BCOM Patch`
* Added `13 Doors of Oblivion+TOTSP Patch` by revenorror
* Added `14 Mines and Caverns MOMW Patch` by Ronik
* Added `15 Perfectly Proficient Parasol Particles Performance Patch` by revenorror
* Added `16 Uvirith's Legacy+BCOM Arena Patch` by Ronik
* Added `17 Uvirith's Legacy+Daedric Shrine Overhaul Molag Bal Patch` by Ronik
* Added `18 Uvirith's Legacy+Daedric Shrine Overhaul Sheogorath Patch` by Ronik
* Added `19 Uvirith's Legacy+OAAB Tel Mora Patch` by Ronik
* Added `20 Netch Bump Mapped Mesh Fix` by Sophie
* Added `21 Imperial Factions Patched` by ffann1998
* Added `22 AFFresh+Adanumuran Reclaimed Patch` by S3ctor
* Added `23 Dynamic Music - Terror of Tel Amur Soundbank` by Ronik
* Added `24 BCOM+Dagon Fel Lighthouse+Remiros' Groundcover Patch` by Remiros
* Added `25 Dynamic Music - Songbook of the North Soundbank` by Ronik
* Added `26 Argonian Full Helms Lore Integrated+Concept Arts plantations+The Wolverine Hall Patch` by Sophie
* Added `27 Little Landscapes Nix Hound Hunting Grounds+Quests for Clans and Vampire Legends Patch` by Sophie
* Added `28 Morrowind Anti-Cheese+Quest Voice Greetings Patch` by Ronik
* Added `29 Better Clothes Complete Shoe Fix` by eddie5
* Added `30 Whiskers Patch for MMH Version All NPCs and Base Replace Fixed Meshes`
* Added `31 Flies Patched` by Half11
* Added `32 OpenMW Fixes For Uvirith's Legacy` by havx, Xandaros, Sophie, and various other authors
* Added `33 AFFresh+Library of Vivec Enhanced Patch` by Ronik
* Added `34 Sophie's Logs On Fire+BCOM Patch` by Sophie
* Added `35 The Forgotten Shields - Artifacts Patched` by S3ctor
* Added `36 Blademeister+Adanumuran Reclaimed Patch` by Ronik
* Added `37 Helm of Tohan Naturalized for Daedric Shrine Overhaul Sheogorath` by Sophie
* Added `38 PPL Negative Light Fix` by Poodlesandwich
* Added `39 Signposts Retextured TR Updated`
* Added `40 NOD+Library of Vivec Enhanced Patch` by Ronik
* Added `41 Blademeister+Daedric Shrine Overhaul Sheogorath Patch` by Ronik

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/24079320)

#### 2.6

* Temporarily removed `04 Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch` as it is not compatible with the latest version of the upstream mod.
* Updated `06 OAABShipwrecks+RRBetterShipsnBoatsPatch` to include a second patch that supports [OAAB Shipwrecks and Justice for Khartag - merged plugin](https://www.nexusmods.com/morrowind/mods/53044).

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/15913205) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.5

* Clarified the usage and compatibility of `UvirithsLegacy+TR Patch`; it likely won't work with Morrowind.exe and is made for a very specific version of the UL+TR patch.
* Added `07 NordicDagonFel+RRBetterShipsnBoatsPatch` which provides compatibility between [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001) and [Nordic Dagon Fel](https://www.nexusmods.com/morrowind/mods/49603).
* Added `06 OAABShipwrecks+RRBetterShipsnBoatsPatch` which provides compatibility between [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001) and [OAAB Shipwrecks](https://www.nexusmods.com/morrowind/mods/51364).

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/15559013) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.4

* Reimplemented `BCOM+Dynamic Distant Buildings` as a direct edit of [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236) rather than a patch on top of it due to various issues that emerged with the patch approach.
* Reimplemented `UvirithsLegacy+TR Patch` without deletes which should improve compatibility.

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/15188036) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.3

* Updated `BCOM+Imperial Towns Revamp Patch` to support BCOM 2.9.4 which contains a change that gives the wheel moved by the patch a unique ID. This means the patch can be more future proof and won't need to be updated for every BCOM release. Huge thanks to RandomPal for doing that!

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/15066976) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.2

* Renamed all `.omwaddon` plugins to `.esp` so that they may better work with tooling such as [Mlox](https://github.com/rfuzzo/mlox).
* Updated `BCOM+Imperial Towns Revamp Patch` to support BCOM 2.9.3.
* Improved and renamed the BCOM+Dynamic Distant Buildings plugin; it now lists Ghastly Glowyfence as a master and should be less suseptible to problems caused by deleting things since it was reimplemented without most deletes.
* `Riharradroon - Path to Kogoruhn v1.0.ESP` is now a master for `04 Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch`, and the masters are all now sorted correctly.

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/15065174) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.1

* Fixed some typos.

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/14965301) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.0

* Each patch has their own small README that gets appended to the `/readme/` page on the website.
* Renamed `01 Redaynia Restored Patch` to `01 Redaynia Restored+Mushroom Tree Replacer Patch` for more consistency with other option namings.
* Added a patch that fixes compatibility between [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) and [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236)'s Ghostgate module.
* Added a patch that fixes compatibility between [Kogoruhn - Extinct City of Ash and Sulfur](https://www.nexusmods.com/morrowind/mods/51615) and [Rise of House Telvanni](https://www.nexusmods.com/morrowind/mods/27545) by disabling various objects when a certain plot point has been reached (the provided image contains spoilers!)
* Added a patch to fix compatibility between [Uvirith's Legacy](https://stuporstar.sarahdimento.com/) and [Tamriel Rebuilt](https://www.tamriel-rebuilt.org/) by deleting an old teleport platform that went to a cell that's since been removed. Additionally, a few walls were deleted to make paths to teleport pads, and a few platforms that went to now-nonexistent cells have been deleted as well.

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/14965209) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 1.0

* Initial release of the mod.

[Download Link](https://gitlab.com/modding-openmw/momw-patches/-/packages/14912246) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)
