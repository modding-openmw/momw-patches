#### 15 Perfectly Proficient Parasol Particles Performance Patch

Formerly available at Nexus mods ([archive link](https://web.archive.org/web/20221119164736/https://www.nexusmods.com/morrowind/mods/48923)). It had open permissions so we're redistributing it here.

##### About

The original description:

This is a straightforward patch to make Melchior Dahrk's mod [Parasol Particles](https://www.nexusmods.com/morrowind/mods/47755) work with Project Atlas' performance friendly atlased textures. My patch covers the Parasol Particles' vanilla style models, as well as the [Markel's Small and Misc Mods Emperor Parasol Replacer](https://www.nexusmods.com/morrowind/mods/42595) and Biont's [Telvanni Bump Maps](https://www.nexusmods.com/morrowind/mods/42431) options. PeterBitt's [Mushroom Tree Replacer](https://www.nexusmods.com/morrowind/mods/45350) is not covered, as it already uses an atlased texture. 

The vanilla models require a parasol texture atlas, which are included with [Project Atlas](https://www.nexusmods.com/morrowind/mods/45399) or [Intelligent Textures](https://www.nexusmods.com/morrowind/mods/47469). Support for custom retextures can be provided by using the parasol BAT file from Project Atlas. For convenience, atlases using the textures for Emperor Parasol Replacer and Telvanni Bump Maps have been included in the download.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

Only load one of the options:

* `00 Project Atlas` should load after Project Atlas
* `00 SWG's Replacer` should load after Emperor Parasol Replacer from Markels Small and Misc Mods
* `00 Telvanni Bump Maps Replacer` should load after Telvanni Bump Maps Replacer
* `00 Vanilla - Smoothed` has no special load requirements

Example using the Project Atlas option:

```
...
data="/home/username/games/openmw/Mods/Performance/ProjectAtlas/00 Core"
...
data="/home/username/games/openmw/Mods/MOMWPatches/15 Perfectly Proficient Parasol Particles Performance Patch/00 Project Atlas"
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/15%20Perfectly%20Proficient%20Parasol%20Particles%20Performance%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
