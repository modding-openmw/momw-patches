#### 05 AFFresh + Samarys Ancestral Tomb Expanded Patch

Patches incompatibilities between [AFFresh](https://www.nexusmods.com/morrowind/mods/53006) and [Samarys Ancestral Tomb Expanded](https://www.nexusmods.com/morrowind/mods/45612).

##### About

Moves objects so that AFFresh and Samarys Ancestral Tomb Expanded are compatible with one another.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CavesAndDungeons/SamarysAncestralTombExpanded"
...
data="/home/username/games/openmw/Mods/Quests/AFFresh"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/05 AFFresh+Samarys Ancestral Tomb Expanded Patch"
...
content=Samarys Ancestral Tomb Expanded.esp
...
content=AFFresh.esp
...
content=AFFresh+SamarysAncestralTombExpanded.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/05%20AFFresh+Samarys%20Ancestral%20Tomb%20Expanded%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
