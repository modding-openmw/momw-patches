#### 25 Dynamic Music - Songbook of the North Soundbank

Plays the music from the mod [Songbook of the North - A music mod for Skyrim Home of the Nords](https://www.nexusmods.com/morrowind/mods/46936) in the locations added by [Skyrim Home Of The Nords](https://www.nexusmods.com/morrowind/mods/44921) using [Dynamic Music (OpenMW Only)](https://www.nexusmods.com/morrowind/mods/53568). Supports the current release of Skyrim Home Of The Nords, and should mostly support the upcoming Markarth Side release.

##### About

Add this to your load order for it to work, there's no special order requirements.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### Load Order

This mod has no special load order requirements.

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/25%20Dynamic%20Music%20-%20Songbook%20of%20the%20North%20Soundbank)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
