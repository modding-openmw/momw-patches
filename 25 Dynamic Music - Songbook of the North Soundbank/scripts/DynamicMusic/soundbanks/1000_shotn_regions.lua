local soundBank = {
    id = 'shotn_regions',
    regionNames = {
        'druadach highlands region',
        'lorchwuir heath region',
        'midkarth region',
        'vorndgad forest region',
        'sundered hills region',
        'solitude forest region',
        'vaalstag highlands region',
        'grey plains region',
        'kilkreath mountains region'
    },
    tracks      = {
        {
            path = 'Skyrim Pack/throat of the world.mp3',
            length = 307
        },
        {
            path = 'Skyrim Pack/warchants.mp3',
            length = 91
        },
        {
            path = 'Skyrim Pack/under the northern stars.mp3',
            length = 181
        },
        {
            path = 'Skyrim Pack/northern blood.mp3',
            length = 172
        },
        {
            path = 'Skyrim Pack/beneath the permafrost.mp3',
            length = 156
        },
    }
}

return soundBank