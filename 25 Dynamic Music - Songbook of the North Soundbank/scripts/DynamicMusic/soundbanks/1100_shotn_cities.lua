local soundBank =      {
  id = 'shotn_cities',
  cellNamePatterns = {
        'Karthwasten',
        'Dragonstar',
        'Karthgad',
        'Ruari',
        'Uramok',
        'Blood Paw Lodge',
        'Even Odds Inn',
        'Mairager',
        'Nargozh',
        'Bailcnoss',
        'Merduibh',
        'Haimtir',
        'Criaglorc',
        'Vorngyd\'s Stand',
        'Markarth Side',
        'Fenhru',
        'Lugarsh',
        'Cairac',
        'Beorinhal',
        'Fenrald\'s Bend',
        'Old Hrol\'dan'
  },
  cellNamePatternsExclude = {
    'Sewer',
    'Dungeon'
  },
    tracks      = {
        {
            path = 'Skyrim Pack/throat of the world.mp3',
            length = 307
        },
        {
            path = 'Skyrim Pack/warchants.mp3',
            length = 91
        },
        {
            path = 'Skyrim Pack/under the northern stars.mp3',
            length = 181
        },
        {
            path = 'Skyrim Pack/northern blood.mp3',
            length = 172
        },
        {
            path = 'Skyrim Pack/beneath the permafrost.mp3',
            length = 156
        },
    }
}

return soundBank
