#### 32 OpenMW Fixes For Uvirith's Legacy

A collection of patches for [Uvirith's Legacy](https://modding-openmw.com/mods/uviriths-legacy/) for use with OpenMW. Includes missing assets from [Necessities of Morrowind](https://www.nexusmods.com/morrowind/mods/52158), patched assets for OpenMW, and a plugin with OpenMW-specific and general bugfixes.

##### About

Original readme is included in the `Readme - OpenMW Fixes for Uviriths Legacy.txt` file.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/PlayerHomes/UvirithsLegacy/Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/32 OpenMW Fixes For Uvirith's Legacy"
...
content=Uvirith's Legacy_3.53.ESP
...
content=UL_3.5_OpenMW_1.3_Add-on.esp
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/32%20OpenMW%20Fixes%20For%20Uvirith's%20Legacy)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
