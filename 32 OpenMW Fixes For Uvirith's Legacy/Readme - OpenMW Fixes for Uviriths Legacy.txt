Changelog:
-----
MOMW edits
-----
Better Bodies fix
    - Gives the assets originally from Better Bodies an unique mesh and texture so that Uvirith's Legacy doesn't overwrite all Better Bodies assets in your game. (by Sophie)

-----
v1.3:
-----
Fixed dependency error
    - synced addon file with Uvirith's Legacy v3.53

Sorkvild the Raven 
    - hair was changed from 'b_n_nord_m_hair01' to 'b_n_nord_m_hair_E2' in the mod
    - that new body part doesn't exist so I reverted the hair back to 'b_n_nord_m_hair01'
    
Skeleton Barrie
    - hair body part 'b_n_skel_uvi_m_hair_01' doesn't exist
    - so I added the body part 'b_n_skel_uvi_m_hair_01' ; pointing to ".\Meshes\bb\b_n_skel_uvi_m_hair_01.nif"
    - which is just the 'B_N_Breton_M_Hair_00.NIF' mesh with the primary ninode flag set to hidden so that no hair is shown

Script DHM_Alchemy_App_Load & DHM_Alchemy_App_UnLoad
    - the "secret master" level apparatus checks will incorrectly remove/add "master" level apparati while all other checks use the matching level
    - edited the scripts to correctly remove/add "secret master" apparati if the player is carrying "secret master" apparati
    - this should fix the issue of hundreds/thousands of apparati appearing in the alchemy table when the player interacts with it
    
Lots of mesh and texture fixes
    - All meshes that were touched have been "cleaned" of orphaned child nodes and empty references
    - Unused texture layers disabled (such as for removed bump maps) and unused nodes removed
    - Pay particular attention to the changes to the AOF potions!!
    
.\Meshes\f\
    Furn_GlassDisplaycase_02.NIF
    - fixed NiAlphaProperty for glass, changed flag from 237 to 4845
    - no discernable visual change from 237 but 4845 is considered a "fixed" version of 237
    - 4845 is considered the correct flag to use with glass such as with a display case
    - should make the transparency uniform across these parts of the mesh
        
    Removed the texture effect and bump map references for the following beds
        st_bed_dl02.nif
        st_bed_dl09.nif
        st_bed_dl11.nif
        st_bed_double_long.nif
        st_bed_r01.nif
        st_bed_r03.nif
        st_bed_r06.nif
        st_bed_r08.nif
        st_bed_r12.nif
        st_bed_dr04.nif
        st_bed_dr07.nif

.\Meshes\kb\
    kb_silver_ltr_opnr_02.nif
    - removed the texture effects and bump map references
        
.\Meshes\KRS\x\
    Ex_TL_sprout.nif
    - OpenMW supports glow maps so I'm including this mesh for anyone who may have downloaded a prior edited version which removed the glow maps
    - Cleaned up some orphaned references

.\Meshes\KRS\d\
    Ex_TL_door_slave.nif
    - removed bump map references
        
.\Meshes\m\
    AOF_potion_*
    - removed bump map references and fixed the NiAlphaProperty for the glass, changed flag from 235 to 4845
    - unlike the alpha flag change with the display case (237 to 4845)
    - going from 235 to 4845 does have a visual change and it's one that I strongly prefer
    - this makes the glass of the potions look like real glass and not some picture of scummy hard water stained faux glass of the original
    - included copies of the fixed meshes with the original alpha flag for those who want the "original" (albeit botched) look of the glass
        
.\Meshes\mel\S\
    mel_chaise01_st.nif
    - removed texture effect

.\Meshes\overlays\
    ovr_*
    - disabled unused texture layers (glow map was enabled but no texture assigned)

.\Meshes\Phijama\
    _Phij_HQ_Dragonis.NIF
    - fixed NiAlphaProperty for the detail textures, changed from 237 to 4844
    - flag 237 was causing the center of the blade to be almost entirely transparent due to how Phijama used texture effects to compose the blade
    - while it's not perfect (still strobes like crazy) it does look much better than original unedited mesh
        
.\Meshes\UVI\
    UA_Tapestry_Telvanni_02.nif
    - fixed the main tapestry source texture reference as it was using an absolute path to "e:\data files\textures\"

    UV_RitualFloor.nif
    - disabled unused texture layers (glow map was enabled but no texture assigned)
            
    Removed bump map references for:
        furn_telv_display_01.nif
        furn_telv_display_02.nif
        in_t_platform.nif
        Light_DarkTelv_Brazier.nif
        Light_Telv_Brazier.nif
        potion_*.nif
        telvanni_coffer_01.nif
        telvanni_coffer_02.nif
        uvi_sign_telvanni.nif
        uvi_switch.nif
        uvi_switch_dark.nif
        
.\Textures\
    Renamed all relevant normal map textures to the appropriate/needed name; 
        aof_potion_glassnm.dds          >>  aof_potion_glass_n.dds
        aof_potion_stemnm.dds           >>  aof_potion_stem_n.dds
        aof_potion_topnm.dds            >>  aof_potion_top_n.dds
        nm_wood_swirlwood_strip_01.dds  >>  kw_wood_swirlwood_strip_01_n.dds
        nm_wood_swirlwood_trim_02.dds   >>  kw_wood_swirlwood_trim_02_n.dds
        st_gold_strip_nm.dds            >>  st_gold_strip_n.dds
        tx_t_display_trim_nm.dds        >>  tx_t_display_trim_n.dds
        tx_t_symbol_stone_nm.dds        >>  tx_t_symbol_stone_n.dds
        tx_t_symbol_stone_slim_nm.dds   >>  tx_t_symbol_stone_slim_n.dds
            
    These normal maps can be used for textures that are a different color so I made a copy and renamed appropriately
        tx_t_sign_telvanni_nm.dds       >>  tx_t_sign_telvanni_n.dds & tx_t_sign_telvanni_red_n.dds
        tx_uvi_potion_nm.dds            >>  tx_uvi_potion_gold_n.dds & tx_uvi_potion_silver_n.dds
            
    The normal map texture names for the letter opener start with "kb" and are located in '.\Textures\kb\'
    However, the diffuse texture names start with "tx" and are stored in '.\Textures\'
    So I copied the normal map textures to the root texture folder and renamed them
        kb_metal_silver_n.dds           >>  tx_metal_silver_n.dds
        kb_metal_silver_plate_01_n.dds  >>  tx_metal_silver_plate_01_n.dds
        kb_metal_silver_strip_02_n.dds  >>  tx_metal_silver_strip_02_n.dds
            
    Lastly, for fun I added normal, height and specular map textures for some of the AOF potion textures
        AOF_potion_cork_n.dds
        AOF_potion_cork_nh.dds
        AOF_potion_cork_spec.dds
        AOF_potion_glass_spec.dds
        AOF_potion_top_nh.dds
        AOF_potion_top_spec.dds
        AOF_potion_twine_n.dds
        AOF_potion_twine_nh.dds
        AOF_potion_twine_spec.dds

-----
v1.2:
-----
The beds in the upper tower are less shiny

-----
v1.1:
-----
The ancient tomes are now usable
The book "Secrets of Dwemer Containment" now gives you the journal entry when read from the inventory, as well. Not just when read from the ground.
Similarly, the scroll you need to read to enchant the Bag of Holding can now be read from the inventory, as well

-----
v1.0:
-----
Inscription ritual can be activated by reading the scrolls
The beds in the lower tower are less shiny