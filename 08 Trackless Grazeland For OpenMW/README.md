#### 08 Trackless Grazeland For OpenMW

[Trackless Grazeland](https://www.nexusmods.com/morrowind/mods/44194) by [R-Zero](https://www.nexusmods.com/morrowind/users/3241081), edited for use with OpenMW.

##### About

Removes the Land Texture Fix 2.0 requirement since OpenMW handles that out of the box.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes (but there's no point)
* `Rebirth`: No

##### Load Order

This mod has no special load order requirements.

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/08%20Trackless%20Grazeland%20For%20OpenMW)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
