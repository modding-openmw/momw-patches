#### 21 Imperial Factions Patched

A patched version of the Imperial Factions mod.

##### About

Fixes several issues found in the [Imperial Factions](https://www.nexusmods.com/morrowind/mods/49231) mod, including:

- Fixed Ranis Athrys not mentioning Ajira when you ask for duties
- Fixed missing space in "faction rank" topic
- Fixed OP faction spells that were made targeted by mistake and should be casted on-self instead, according to Danae.
- Fixed Jim Stacey not giving you the Speak With Percius quest
- Fixed Sugar-Lips Habasi's missing dialogue line after completion of The Vintage Brandy quest
- Fixed Aengoth the Jeweler's missing dialogue line after completion of several of his quests
- Fixed Skink-in-Tree's-Shade's missing dialogue line after completion of several of his quests

Author of the patch: **[ffann1998](https://www.nexusmods.com/morrowind/users/6073190)**

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/GuildsFactions/ImperialFactions"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/21 Imperial Factions Patched"
...
content=Imperial Factions.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/21%20Imperial%20Factions%20Patched)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
