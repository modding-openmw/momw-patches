#### 27 Little Landscapes Nix Hound Hunting Grounds + Quests for Clans and Vampire Legends Patch

A patched version of [Little Landscapes - Nix Hound Hunting Grounds](https://www.nexusmods.com/morrowind/mods/53333) that fixed an incompatibility with [Quests for Clans and Vampire Legends (QCVL)](https://www.nexusmods.com/morrowind/mods/49486).

##### About

Replaces the original Little Landscapes - Nix Hound Hunting Grounds mod.

Makes the entrance to the Selaro Ancestral Tomb accessible.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/GuildsFactions/QuestsforClansandVampireLegendsQCVL\Quests for Clans and Vampire Legends\Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/27 Little Landscapes Nix Hound Hunting Grounds+Quests for Clans and Vampire Legends Patch"
...
content=Quests for Clans and Vampire Legends.ESP
...
content=Little Landscape - Nix Hunting Ground.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/27%20Little%20Landscapes%20Nix%20Hound%20Hunting%20Grounds+Quests%20for%20Clans%20and%20Vampire%20Legends%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
