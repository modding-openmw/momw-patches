#### 53 Mages Guild Stronghold Nchagalelft OpenMW Patch

Fixes issues with [Mages Guild Stronghold - Nchagalelft](https://www.nexusmods.com/morrowind/mods/53342) observed on MOMW's Total Overhaul modlist.

##### About

Changes the scripts that are supposed to enable and disable various objects around Nchagalelft to no longer use the *CellChanged* function, but the *GetDisabled* function. For some users, the scripts didn't fire at all, which is possibly caused by the fact that there's so many local scripts running in the interior that they "miss the window" where the condition is true.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes (most likely not necessary)
* `Rebirth`: Yes

##### Load Order

```
data="/home/username/games/openmw/Mods/PlayerHomes/MagesGuildStrongholdNchagalelft/Building up Nchagalelft"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/53 Mages Guild Stronghold Nchagalelft OpenMW Patch"
...
content=MGO Building Up Nchagalelft.esp
...
content=MGOBuildingUpNchagalelft_OpenMWPatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/53%20Mages%20Guild%20Stronghold%20Nchagalelft%20OpenMW%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)

