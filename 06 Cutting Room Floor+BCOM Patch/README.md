#### 06 Cutting Room Floor + BCoM Patch

Fixes incompatibilities between the Jobs module from [Cutting Room Floor](https://www.nexusmods.com/morrowind/mods/47307) and [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231).

##### About

Moves a chest added by Cutting Room Floor so that it doesn't bleed into an object added by Beautiful Cities of Morrowind in the **Sadrith Mora, Fara's Hole in the Wall** interior.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CutContent/CuttingRoomFloor/Data Files"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/06 Cutting Room Floor+BCOM Patch"
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/00 Core"
...
content=Cutting Room Floor - Jobs.esp
...
content=CRFJobs_BCoM_patch.esp
...
content=Beautiful cities of Morrowind.ESP

...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/06%20Cutting%20Room%20Floor+BCOM%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
<!-- * [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->
