#### 01 OAAB Grazelands+Mines and Caverns Patch

Fixes a compatibility issue between [OAAB Grazelands](https://www.nexusmods.com/morrowind/mods/49075) and [Mines and Caverns](https://www.nexusmods.com/morrowind/mods/44893).

##### About

Moves a quest item from OAAB Grazelands to be accessible after the changes made by Mines and Caverns to the layout of the cavern Pinsun.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CitiesTowns/OAABGrazelands/00 Core"
...
data="/home/username/games/openmw/Mods/CavesandDungeons/MinesandCaverns/Mines and Caverns"
...
data="/home/username/games/openmw/Mods/Patches/MOMWPatches/01 OAAB Grazelands+Mines and Caverns Patch"
...
content=OAAB_Grazelands.ESP
...
content=Clean_Mines and Caverns.esp
...
content=OAAB_Grazelands_Mines_and_Caverns_patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/momw-patches/-/tree/master/01%20OAAB%20Grazelands%2BMines%20and%20Caverns%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/momw-patches/)
